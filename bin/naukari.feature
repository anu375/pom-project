Feature: Naukari Register Functionality
  Scenario: User wants to register onto Naukari
  Given user is on homepage of naukari.com
  When  user will click on register now
  Then title of page will be Register on Naukri.com: Apply to Millions of Jobs Online
  When user will click on I am a fresher
  Then title of page will be  Post Resume Online - Submit your CV - Naukri.com
  When user will fill all the details and click on register now
  Then registeration will be successfully done
  