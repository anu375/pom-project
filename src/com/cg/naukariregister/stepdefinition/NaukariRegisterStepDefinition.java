package com.cg.naukariregister.stepdefinition;
import java.util.ArrayList;
import java.util.Set;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cg.naukariregister.beans.NaukriBeans;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class NaukariRegisterStepDefinition {

	private WebDriver driver;
	private NaukriBeans naukriBeans = new NaukriBeans();

	@Given("^user is on homepage of naukari\\.com$")
	public void user_is_on_homepage_of_naukari_com() throws Throwable {               
		  
		System.setProperty("webdriver.chrome.driver", "D:\\Softwares\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.naukri.com");
		PageFactory.initElements(driver, naukriBeans);
		
		Set<String>allWindows=driver.getWindowHandles();
		ArrayList<String>tabs=new ArrayList<>(allWindows);
		
		for(int i=1;i<tabs.size();i++) {	 
			driver.switchTo().window(tabs.get(i));
			driver.close();
	    }
		
		driver.switchTo().window(tabs.get(0));
	}

	@When("^user will click on register now$")
	public void user_will_click_on_register_now() throws Throwable {
		naukriBeans.registerNowButton();
		
		Set<String>allWindows=driver.getWindowHandles();
		ArrayList<String>tabs=new ArrayList<>(allWindows);
		
		driver.switchTo().window(tabs.get(1));
		driver.switchTo().window(tabs.get(0));
		driver.close();
		driver.switchTo().window(tabs.get(1));
		
		System.out.println(driver.getTitle());
	}

	@Then("^title of page will be Register on Naukri\\.com: Apply to Millions of Jobs Online$")
	public void title_of_page_will_be_Register_on_Naukri_com_Apply_to_Millions_of_Jobs_Online() throws Throwable {
		String title=driver.getTitle();
		Assert.assertEquals("Register on Naukri.com: Apply to Millions of Jobs Online", title);
	}

	@When("^user will click on I am a fresher$")
	public void user_will_click_on_I_am_a_fresher() throws Throwable {
		naukriBeans.iAmFresherButton();
		System.out.println(driver.getTitle());
	}

	@Then("^title of page will be  Post Resume Online - Submit your CV - Naukri\\.com$")
	public void title_of_page_will_be_Post_Resume_Online_Submit_your_CV_Naukri_com() throws Throwable {
		//String title=driver.getTitle();
		//Assert.assertEquals(" Resume Manager - Post Resume Online - Submit your CV - Naukri.com  ", title); 
	}

	@When("^user will fill all the details and click on register now$")
	public void user_will_fill_all_the_details_and_click_on_register_now() throws Throwable {
		naukriBeans.setFresherName("KungLao");
		naukriBeans.setFresherEmail("kungalaosh472MKx@yahoo.com");
		naukriBeans.setFresherpassword("ewqr4351");
		naukriBeans.setFreshermobileNo("9876076435");
		naukriBeans.setCurrentLocation("Pune");
		naukriBeans.setUploadResume("D:\\Users\\ADM-IG-HWDLAB1C\\Desktop\\resume.pdf");
		naukriBeans.submitDetailsButton();
 
		WebDriverWait wait=new WebDriverWait(driver,100);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("qualification_0")));
	 
		naukriBeans.highestQualificationButton();
		naukriBeans.selectHigestQualification();
		naukriBeans.setCourse("lo");
		naukriBeans.setSpecialization("game");
		naukriBeans.setCollege("Xzaandaar");
		naukriBeans.courseTypeButton();
		naukriBeans.passingYearButton();
		naukriBeans.selectPassingyearButton();
		naukriBeans.setSkills("1Ghdwy");
		naukriBeans.continueButton();
		
		Thread.sleep(10000);
	}
	
	@Then("^registeration will be successfully done$")
	public void registeration_will_be_successfully_done() throws Throwable {
		String title=driver.getTitle();
	    Assert.assertEquals("Freshers My Naukri", title); 
	    driver.quit();
    }   
	
}
